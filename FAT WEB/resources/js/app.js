window.Vue = require('vue').default;
window.axios = require('axios');

import Multiselect from 'vue-multiselect'

// require('./bootstrap');



Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Rutas Layouts

Vue.component('menu-principal', require('./components/layouts/MenuComponent.vue').default);
Vue.component('dashboard', require('./components/layouts/DashboardComponent.vue').default);

// admin
Vue.component('permissions', require('./components/permisos/permisosComponent.vue').default);
Vue.component('role', require('./components/roles/roleComponent.vue').default);
Vue.component('user', require('./components/usuarios/userComponent.vue').default);
Vue.component('tables', require('./components/usuarios/dataTablesComponent.vue').default);

// sessiones 
Vue.component('sessiones', require('./components/usuarios/sessionesComponent.vue').default);

//clients
Vue.component('clients', require('./components/clientes/clientsComponent.vue').default);
Vue.component('tablesclients', require('./components/clientes/dataTablesComponent.vue').default);
Vue.component('catalogo', require('./components/catalogo/catalogoComponent.vue').default);

// pedidos
Vue.component('pedidos', require('./components/pedidos/PedidosComponent.vue').default);


// Productos
Vue.component('listas', require('./components/productos/dataTablesComponent.vue').default);
Vue.component('productos', require('./components/productos/ProductosComponent.vue').default);




const app = new Vue({
    el: '#app',
});
