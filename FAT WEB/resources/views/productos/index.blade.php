@extends('layouts.app')
@section('content-layouts')
	<productos :marcas = "{{ json_encode($marcas) }}" :peliculas = "{{ json_encode($peliculas) }}" :estados = "{{ json_encode($estados) }}" ></productos>
	<listas :marcas = "{{ json_encode($marcas) }}" :productos="{{json_encode($list) }}" :peliculas = "{{ json_encode($peliculas) }}" :estados = "{{ json_encode($estados) }}" ></listas>
@endsection
	