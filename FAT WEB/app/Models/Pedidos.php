<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    use HasFactory;

    protected $table = 'pedidos';
    protected $fillable = ['producto_id','user_id','estado_id', 'fechaInicio','fechaFin'];
    protected $guarded = ['id'];

    // relacion  productos
    public function producto()
    {
        return $this->hasOne(Productos::class,'id','producto_id');
    }
    // relacion usuario  
    public function usuario()
    {
        return $this->hasOne(User::class,'id','user_id');
    }
    // relacion estados  
    public function estado()
    {
        return $this->hasOne(Estados::class,'id','estado_id');
    }
}
