<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	public function run()
    {
       $this->call(RolesSeeder::class);
       $this->call(UserSeeder::class);
       $this->call(MarcasSeeder::class);
       $this->call(EstadosSeeder::class);
       $this->call(PeliculasSeeder::class);
       $this->call(ProductosSeeder::class);
    }
}
