<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Productos;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Productos::create([
             'nombre' => 'K1000',
             'modelo' => 'K1000',
             'description' => 'El cuerpo totalmente metálico le da un peso saludable, y la simplicidad de sus funciones',
             'soporte' => 'Si', 
             'valor' => '20.000',
             'pelicula_id' => '1',
             'marca_id' => '6',
             'estado_id' => '3'
         ]);
        Productos::create([
             'nombre' => 'Leica',
             'modelo' => 'M6',
             'description' => 'cuando se trata de fotografiar, es una de las cámaras más buscadas de todos los tiempos',
             'soporte' => 'Si', 
             'valor' => '25.000',
             'pelicula_id' => '4', 
             'marca_id' => '2',
             'estado_id' => '3'
         ]);
        Productos::create([
             'nombre' => 'Olympus',
             'modelo' => 'OM-1',
             'description' => 'increíble calidad y sus cuerpos ligeros y compactos se caracteriza  por su alta definición',
             'soporte' => 'No', 
             'valor' => '40.000',
             'pelicula_id' => '5', 
             'marca_id' => '4',
             'estado_id' => '3'
         ]);
        Productos::create([
             'nombre' => 'Canonet',
             'modelo' => 'G III QL17',
             'description' => 'increíble calidad y sus cuerpos ligeros y compactos se caracteriza  por su alta definición',
             'soporte' => 'No', 
             'valor' => '55.000',
             'pelicula_id' => '6', 
             'marca_id' => '4',
             'estado_id' => '3'
         ]);
        Productos::create([
             'nombre' => 'Minolta',
             'modelo' => 'X-700',
             'description' => 'increíble calidad y sus cuerpos ligeros y compactos se caracteriza  por su alta definición',
             'soporte' => 'Si',
             'valor' => '60.000',
             'pelicula_id' => '3', 
             'marca_id' => '4',
             'estado_id' => '4'
         ]);
        Productos::create([
             'nombre' => 'Nikon',
             'modelo' => 'FM2',
             'description' => 'increíble calidad y sus cuerpos ligeros y compactos se caracteriza  por su alta definición',
             'soporte' => 'No', 
             'valor' => '38.000',
             'pelicula_id' => '2', 
             'marca_id' => '4',
             'estado_id' => '6'
         ]);
    }
}
