@extends('layouts.app')
@section('content-layouts')
	<role :permission="{{ json_encode($permission) }}"></role>
@endsection