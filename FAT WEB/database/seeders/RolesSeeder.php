<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    public function run()
    {
        // roles 
        $admin =  Role::create(['name' => 'admin', 'description' => 'Rol Administrador']);
    	$cliente =  Role::create(['name' => 'cliente','description' => 'Rol Cliente']);
    	// permisos
        Permission::create(['name' => 'admin','description' => 'Permiso general admin'])->assignRole($admin);
        Permission::create(['name' => 'catalogo','description' => 'Permiso para ver catalogo'])->assignRole($cliente);
    }
}
