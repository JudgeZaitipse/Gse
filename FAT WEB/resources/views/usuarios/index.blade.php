@extends('layouts.app')
@section('content-layouts')
	<user :role = "{{ json_encode($role) }}" ></user>
	<tables :users = "{{ json_encode($users) }} " :role = "{{ json_encode($role) }}"></tables>
@endsection
	