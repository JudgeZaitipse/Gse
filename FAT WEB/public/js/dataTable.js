$(document).ready(function() {
  $('#datatables').DataTable({
    "pagingType": "full_numbers",
    "lengthMenu": [
      [10, 25, 50, -1],
      [10, 25, 50, "All"]
    ],
    responsive: true,
    language: {
      search: "Buscar",
      searchPlaceholder: "...",
      sLengthMenu:    "Mostrar _MENU_ registros",
      sZeroRecords:   "No se encontraron resultados",
      sEmptyTable:    "Ningún dato disponible en esta tabla",
      sInfo:          "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
      sInfoEmpty:     "Mostrando registros del 0 al 0 de un total de 0 registros",
      oPaginate: {
          "sFirst":    "Primero",
          "sLast":    "Último",
          "sNext":    "Siguiente",
          "sPrevious": "Anterior"
      }, 
    }
  });  
});