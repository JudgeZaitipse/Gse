
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{ asset('assets/css/material-dashboard.css?v=2.1.2') }}" rel="stylesheet" />
    <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
</head>
<body>
<div class="off-canvas-sidebar">
  <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
    <div class="container">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="/">Registro</a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
      {{-- menu  --}}
      @if (Route::has('login'))
         <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            <div class="collapse navbar-collapse justify-content-end">
              <ul class="navbar-nav">
                @auth
                {{-- Home --}}
                  <li class="nav-item">
                    <a href="{{ url('/home') }}" class="nav-link">
                      <i class="material-icons">dashboard</i>
                      INICIO
                    </a>
                  </li>
                @else
                  {{-- Login --}}
                  <li class="nav-item ">
                    <a href="{{ route('login') }}" class="nav-link">
                      <i class="material-icons">fingerprint</i>
                      INICIO SESIÓN
                    </a>
                  </li>
                  {{-- registro --}}
                  @if (Route::has('register'))
                      <li class="nav-item ">
                        <a href="{{ route('register') }}" class="nav-link">
                          <i class="material-icons">person_add</i>
                          REGISTRO
                        </a>
                      </li>
                  @endif
                @endauth
              </ul>
            </div>
          </div>
      @endif
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../img/fondos/background.jpeg');background-repeat: no-repeat; background-size: cover;">
    <div class="container">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">
            <div class="card card-signup">
              <h2 class="card-title text-center">Registrarse</h2>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-5 ml-auto">
                    <div class="info info-horizontal">
                      <div class="mx-auto mt-4">
                        <center>
                            <img class="mt-5" src="{{ asset('img/logos/logo.png') }}" width="330" height="auto">
                        </center>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5 mr-auto">
                    <div class="social text-center">
                      <button class="btn btn-just-icon btn-round btn-twitter">
                        <i class="fa fa-twitter"></i>
                      </button>
                      <button class="btn btn-just-icon btn-round btn-dribbble">
                        <i class="fa fa-dribbble"></i>
                      </button>
                       <button class="btn btn-just-icon btn-round btn-twitter">
                        <i class="fa fa-facebook"> </i>
                      </button>
                      <h4 class="mt-3">Información de usuario</h4>
                    </div>
                    <form class="form" method="POST" action="{{ route('register') }}">
                        @csrf
                      <div class="form-group has-default">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">face</i>
                            </span>
                          </div>
                           <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre ...">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="form-group has-default">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">mail</i>
                            </span>
                          </div>
                          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Correo ...">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="form-group has-default">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                              <i class="material-icons">lock_outline</i>
                            </span>
                          </div>
                          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Contraseña ...">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                      </div>
                      <div class="form-group has-default">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="material-icons">lock_outline</i>
                            </span>
                          </div>
                           <input id="password-confirm" type="password" placeholder="Confirmar Contraseña" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>
                      </div>
                    
                      <div class="form-check">
                        <label class="form-check-label">
                          <input class="form-check-input" type="checkbox" value="" checked="">
                          <span class="form-check-sign">
                            <span class="check"></span>
                          </span>
                          Acepto 
                          <a href="#">terminos y condiciones</a>.
                        </label>
                      </div>
                      <div class="text-center">
                        <button type="submit" class="btn btn-rose btn-round mt-4">Registrarse</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://woobsing.com/" target="_blank">
                  GSE 
                </a>
              </li>
              <li>
                <a href="https://woobsing.com/sobre-nosotros" target="_blank">
                  Nosotros
                </a>
              </li>
              <li>
                <a href="https://woobsing.com/blog-2/blog" target="_blank">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://woobsing.com/contacto" target="_blank">
                  Contacto
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Todos los derecho reservados.
          </div>
        </div>
      </footer>
    </div>
  </div>
</div>
  <!--   JS    -->
  <script src=" {{ asset('assets/js/core/jquery.min.js') }}"></script>
  <script src=" {{ asset('assets/js/core/popper.min.js') }}"></script>
  <script src=" {{ asset('assets/js/core/bootstrap-material-design.min.js') }}"></script>
  <script src=" {{ asset('assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <script src=" {{ asset('assets/js/plugins/chartist.min.js') }}"></script>
  <script src=" {{ asset('assets/js/plugins/bootstrap-notify.js') }}"></script>
  <script src=" {{ asset('assets/js/material-dashboard.min.js?v=2.2.2') }}" type="text/javascript"></script>
</body>
</html>