<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',70)->nullable(false);
            $table->string('modelo',100)->nullable(false);
            $table->enum('soporte',['Si','No']);
            $table->string('description',250)->nullable(true);
            $table->string('valor',15)->nullable(true);
            $table->string('marca_id',100)->nullable(false);
            $table->string('pelicula_id',100)->nullable(false);
            $table->string('estado_id',100)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
