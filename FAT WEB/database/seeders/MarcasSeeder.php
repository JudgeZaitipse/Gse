<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Marcas;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Marcas::create(['nombre' => 'Nikon','direccion' => 'Crr 1 G #45-21']);
        Marcas::create(['nombre' => 'Canon eos','direccion' => 'Kr 68 A #12-01']);
        Marcas::create(['nombre' => 'Fujifilm','direccion' => 'Carrera 1 G #68b 01']);
        Marcas::create(['nombre' => 'Sony','direccion' => 'Av 1 e #77-23']);
        Marcas::create(['nombre' => 'Panasonic','direccion' => 'Diag 45 f #12-45']);
        Marcas::create(['nombre' => 'Pentax','direccion' => 'carrera 7 #67-37']);
        Marcas::create(['nombre' => 'Olympus','direccion' => 'kr 68 q #12-44b sur']);
    }
}