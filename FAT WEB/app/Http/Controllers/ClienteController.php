<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    // index cliente
    public function index()
    {
        return view('clientes.index');
    }
    //  metodo list cleentes
    public function show()
    {
        $clients = Cliente::all();
        if (count($clients) > 0) {
            return [
                     'status'  => 200,
                     'clients' => $clients,
                    ];
        }else{
            return [
                     'status'  => 404,
                     'clients' => '',
                    ];
        }
    }

    // creacion cliente
    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'name'      => 'required|string',
            'email'     => 'required|email',
            'document'  => 'required|numeric',
            'address'   => 'required|string',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $find = Cliente::where('document',$request->document)->first();
            if (isset($find)) {
                return 1;
            }else{
                $client = new Cliente;
                $client->name = $request->name;
                $client->email = $request->email;
                $client->document = $request->document;
                $client->address = $request->address;
                $save = $client->save();
                if($save){
                    return 200;
                }else{
                    return 500;
                }
            }
        }
    }

    // modificar cliente
    public function update(Request $request, Cliente $cliente)
    {
        $v = \Validator::make($request->all(), [
            'name'      => 'required|string',
            'email'     => 'required|email',
            'document'  => 'required|numeric',
            'address'   => 'required|string',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $client = Cliente::find($request->id);
            if (isset($client)) {
                $client->name = $request->name;
                $client->email = $request->email;
                $client->document = $request->document;
                $client->address = $request->address;
                $save = $client->update();
                if($save){
                    return 200;
                }else{
                    return 500;
                }
            }else{
                return 1;
            }
        }
    }

    // eliminar cliente
    public function destroy($client)
    {
        $client = Cliente::find($client);
        if(!empty($client)){
            $client->delete();
            return 200;
        }else{
            return 500;
        }
    }
}
