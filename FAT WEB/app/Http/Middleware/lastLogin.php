<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class lastLogin
{
    public function handle(Request $request, Closure $next)
    {
       $dia = date("d-m-Y");
       $last =   date('d-m-Y',strtotime(auth()->user()->last_login));

       if ( $dia == $last  ){
            return $next($request);
       }else{
             return redirect('/sesiones');
       }

    }
}
