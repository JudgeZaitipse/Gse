<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>{{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="{{ asset('assets/css/material-dashboard.css?v=2.1.2') }}" rel="stylesheet" />
    <link href="{{ asset('assets/demo/demo.css') }}" rel="stylesheet" />
</head>
<body>
<div class="off-canvas-sidebar">
  <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
    <div class="container">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="/">Inicio Sesión</a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
      {{-- menu  --}}
      @if (Route::has('login'))
          <div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
            <div class="collapse navbar-collapse justify-content-end">
              <ul class="navbar-nav">
                @auth
                {{-- Home --}}
                  <li class="nav-item">
                    <a href="{{ url('/home') }}" class="nav-link">
                      <i class="material-icons">dashboard</i>
                      INICIO
                    </a>
                  </li>
                @else
                  {{-- Login --}}
                  <li class="nav-item ">
                    <a href="{{ route('login') }}" class="nav-link">
                      <i class="material-icons">fingerprint</i>
                      INICIO SESIÓN
                    </a>
                  </li>
                  {{-- registro --}}
                  @if (Route::has('register'))
                      <li class="nav-item ">
                        <a href="{{ route('register') }}" class="nav-link">
                          <i class="material-icons">person_add</i>
                          REGISTRO
                        </a>
                      </li>
                  @endif
                @endauth
              </ul>
            </div>
          </div>
      @endif
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" filter-color="black" style="background-image: url('../img/fondos/background.jpeg');background-repeat: no-repeat; background-size: cover;">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-sm-8 ml-auto mr-auto">
            <form class="form" method="POST" action="{{ route('login') }}">
              @csrf  
              <div class="card card-login card-hidden">
                <div class="card-header card-header-primary text-center">
                  <h4 class="card-title mt-2 ">Inicio Sesión</h4>
                  <div class="social-line">
                    <a href="#" class="btn btn-just-icon btn-link btn-white">
                      <i class="fa fa-facebook-square"></i>
                    </a>
                    <a href="#" class="btn btn-just-icon btn-link btn-white">
                      <i class="fa fa-twitter"></i>
                    </a>
                    <a href="#" class="btn btn-just-icon btn-link btn-white">
                      <i class="fa fa-google-plus"></i>
                    </a>
                  </div>
                </div>
                <div class="card-body ">
                  <p class="card-description text-center">Datos de usuario</p>
                  <p class="card-description text-center">
                     @if ($errors->any())
                        <div class="alert alert-danger">
                          <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                          </ul>
                        </div>
                      @endif
                  </p>
                  {{-- Usuario --}}  
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">face</i>
                        </span>
                      </div>
                      {{-- <input type="email" class="form-control" placeholder="Usuario"> --}}
                      <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Usuario...">
                       @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                  </span>
                  {{-- Contraseña --}}
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                       <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Contraseña...">
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                      {{-- <input type="password" class="form-control" placeholder="Contraseña"> --}}
                    </div>
                  </span>
                  <span class="bmd-form-group">
                    <div class="form-group row">
                        <div class="col-md-12 ">
                            <div class="form-check">
                               <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label class="form-check-label" for="remember">
                                    {{ __('Recordar') }}
                                </label>
                                <span class="form-check-sign">
                                  <span class="check"></span>
                              </span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            @if (Route::has('password.request'))
                              <a class="btn btn-link" href="{{ route('password.request') }}">
                                  {{ __('OLVIDE MI CONTRASEÑA') }}
                              </a>
                            @endif
                        </div>
                    </div>
                  </span>
                </div>
                <div class="card-footer justify-content-center">
                  <input type="submit" class="btn btn-primary btn-link btn-lg" value="Ingresar">  
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://woobsing.com/" target="_blank">
                  GSE 
                </a>
              </li>
              <li>
                <a href="https://woobsing.com/sobre-nosotros" target="_blank">
                  Nosotros
                </a>
              </li>
              <li>
                <a href="https://woobsing.com/blog-2/blog" target="_blank">
                  Blog
                </a>
              </li>
              <li>
                <a href="https://woobsing.com/contacto" target="_blank">
                  Contacto
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, Todos los derecho reservados.
          </div>
        </div>
      </footer>
    </div>
  </div>
</div>
  <!--   JS    -->
  <script src=" {{ asset('assets/js/core/jquery.min.js') }}"></script>
  <script src=" {{ asset('assets/js/core/popper.min.js') }}"></script>
  <script src=" {{ asset('assets/js/core/bootstrap-material-design.min.js') }}"></script>
  <script src=" {{ asset('assets/js/plugins/perfect-scrollbar.min.js') }}"></script>
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <script src=" {{ asset('assets/js/plugins/chartist.min.js') }}"></script>
  <script src=" {{ asset('assets/js/plugins/bootstrap-notify.js') }}"></script>
  <script src=" {{ asset('assets/js/material-dashboard.min.js?v=2.2.2') }}" type="text/javascript"></script>
</body>
</html>