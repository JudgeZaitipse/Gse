<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;

    protected $table = 'productos';
    protected $fillable = ['nombre','marca_id','modelo','description','soporte','estado_id','pelicula_id'];
    protected $guarded = ['id'];

    // relacion marcas 
    public function marcas()
    {
        return $this->hasOne(Marcas::class,'id','marca_id');
    }
    // estados 
    public function estados()
    {
        return $this->hasOne(Estados::class,'id','estado_id');
    }
    // peliculas 
    public function peliculas()
    {
        return $this->hasOne(Peliculas::class,'id','pelicula_id');
    } 

}
