<div class="sidebar" data-color="rose" data-background-color="black" data-image="../assets/img/sidebar-1.jpg">
      <div class="logo">
        <a href="{{ route('home') }}" class="simple-text logo-normal">
           <center>Menu</center>  
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item active ">
            <a class="nav-link" href="{{ route('dashboard') }}">
              <i class="material-icons">dashboard</i>
              <p> Dashboard </p>
            </a>
          </li>
          {{-- Administracion --}}
           <li class="nav-item ">
            <a class="nav-link" data-toggle="collapse" href="#pagesExamples">
              <i class="material-icons">manage_accounts</i>
              <p> Administración
                <b class="caret"></b>
              </p>
            </a>
            <div class="collapse" id="pagesExamples">
              <ul class="nav">
                @hasanyrole('admin|admin')
                {{-- users --}}
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('usuarios.index') }}">
                    <span class="sidebar-mini"><i class="material-icons">supervised_user_circle</i></span>
                    <span class="sidebar-normal">Usuarios</span>
                  </a>
                </li>
                {{-- roles --}}
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('roles.index') }}">
                    <span class="sidebar-mini">R</span>
                    <span class="sidebar-normal">Roles</span>
                  </a>
                </li>
                {{-- permissions --}}
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('permisos.index') }}">
                    <span class="sidebar-mini">P</span>
                    <span class="sidebar-normal">Permisos</span>
                  </a>
                </li>
                {{-- Clients  --}}
                <li class="nav-item ">
                  <a class="nav-link" href="{{ route('clientes.index') }}">
                    <span class="sidebar-mini">C</span>
                    <span class="sidebar-normal">Clientes</span>
                  </a>
                </li>
                {{-- produtos --}}
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('productos.index') }}">
                    <span class="sidebar-mini"> P </span>
                    <span class="sidebar-normal"> Productos </span>
                  </a>
                </li>
                @endhasanyrole
                @hasanyrole('admin|cliente')
                {{-- catalogo --}}
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('catalogo.index') }}">
                    <span class="sidebar-mini"> P </span>
                    <span class="sidebar-normal"> Catálogo </span>
                  </a>
                </li>
                {{-- pedidos --}}
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('pedidos.index') }}">
                    <span class="sidebar-mini"> P </span>
                    <span class="sidebar-normal"> Pedidos </span>
                  </a>
                </li>
                @endhasanyrole
              </ul>
            </div>
          </li>
          {{-- End  Administracion  --}}
        </ul>
      </div>
      <div class="sidebar-background"></div>
</div>