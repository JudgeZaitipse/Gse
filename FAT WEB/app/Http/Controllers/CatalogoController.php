<?php

namespace App\Http\Controllers;

use App\Models\Catalogo;
use Illuminate\Http\Request;
use App\Models\Productos;
use App\Models\Pedidos;
use App\Models\Estados;
use Auth;

class CatalogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Productos::with(['marcas','estados','peliculas'])->get();
        return view('catalogos.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'id'          => 'required',
            'producto'    => 'required'
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $dia = date('Y-m-d');
            $past = strtotime('+7 day', strtotime($dia));
            $past = date('Y-m-d', $past);
            $estado = Estados::where('nombre','like','%Alquilado%')->first();
            $find = Pedidos::where('user_id',Auth::user()->id)
            ->whereBetween('fechaFin', [$dia, $past])->get();
            if (count($find) > 0) {
                return 1;
            }else{
                $pedidos = new Pedidos;
                $pedidos->producto_id = $request->producto['id'];
                $pedidos->user_id = Auth::user()->id;
                $pedidos->estado_id = $estado->id;
                $pedidos->fechaInicio = $dia;
                $pedidos->fechaFin = $past;
                $save = $pedidos->save();
                if($save){
                    return 200;
                }else{
                    return 500;
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function show(Catalogo $catalogo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function edit(Catalogo $catalogo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Catalogo $catalogo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Catalogo  $catalogo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $find = Pedidos::find($id);
        if(!empty($find)){
            $find->delete();
            return 200;
        }else{
            return 500;
        }
    }
}
