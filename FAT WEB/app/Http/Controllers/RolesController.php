<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Request;
use App\Models\Roles;

class RolesController extends Controller
{
    use HasRoles;
    
    public function index()
    {
        $permission = Permission::select('id','name')->get(); 
        return view('roles.index')->with(['permission' => $permission]);
    }

    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'name'           => 'required|string',
            'descripcion'    => 'required|string',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{

            $find = Role::where('name', 'like',  $request->name)->first();
            if (isset($find)) {
                return 1;
            }else{
                $rol = new Role;
                $rol->name = $request->name;
                $rol->description = $request->descripcion;
                $save = $rol->save();
                if($save){
                    foreach ($request->permission as $key => $value) {
                        $rol->givePermissionTo($value['name']);
                    }
                    return 200;
                }else{
                    return 500;
                }
            }
        }
    }

    public function show(Roles $roles)
    {
        //
    }

    public function edit(Roles $roles)
    {
        //
    }


    public function update(Request $request, Roles $roles)
    {
        //
    }

    public function destroy(Roles $roles)
    {
        //
    }
}
