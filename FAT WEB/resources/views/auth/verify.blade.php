@extends('auth.check')
@section('content-layouts')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h4><strong>Verifique su dirección de correo electrónico</strong></h4>
                </div>
                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.
                        </div>
                    @endif
                    <p>
                        <strong>
                            Antes de continuar, verifique su correo electrónico para ver si hay un enlace de verificación.                        
                        </strong>
                    </p>
                    <span>
                        <strong>Si no recibió el correo electrónico,</strong>
                    </span>    
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">haga clic aquí para solicitar otro</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection





