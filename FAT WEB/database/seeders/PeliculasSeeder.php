<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Peliculas;

class PeliculasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Peliculas::create([
             'nombre' => 'Color Plus',
             'marca' => 'kodak',
             'sensibilidad' => 'asa',
             'iso' => '200',
             'formato' => '35mm' 
         ]);
        Peliculas::create([
             'nombre' => 'Color Plus',
             'marca' => 'kodak',
             'sensibilidad' => 'asa',
             'iso' => '400',
             'formato' => '110mm' 
         ]);
        Peliculas::create([
             'nombre' => 'Color Plus',
             'marca' => 'kodak',
             'sensibilidad' => 'asa',
             'iso' => '1600',
             'formato' => '120mm' 
         ]);
        Peliculas::create([
             'nombre' => 'Indoor & outdoor',
             'marca' => 'Fujifilm',
             'sensibilidad' => 'asa',
             'iso' => '200',
             'formato' => '35mm' 
         ]);
         Peliculas::create([
             'nombre' => 'Indoor & outdoor',
             'marca' => 'Fujifilm',
             'sensibilidad' => 'asa',
             'iso' => '600',
             'formato' => '110mm' 
         ]);
          Peliculas::create([
             'nombre' => 'Indoor & outdoor',
             'marca' => 'Fujifilm',
             'sensibilidad' => 'asa',
             'iso' => '1600',
             'formato' => '120mm' 
         ]);
        
    }
}
