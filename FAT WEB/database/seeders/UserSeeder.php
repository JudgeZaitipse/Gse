<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => '', // por favor ingresar un correo real 
            'password' => Hash::make('12345'),
        ])->assignRole('admin');

        User::create([
            'name' => 'Usuario',
            'email' => '', // por favor ingresar un correo real 
            'password' => Hash::make('12345'),
        ])->assignRole('cliente');
    }
}
