<?php

namespace App\Http\Controllers;

use App\Models\permisos as Permisos;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;

class PermisosController extends Controller
{
   
    public function index()
    {
       return view('permisos.index');
    }

    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'name'           => 'required|string',
            'description'    => 'required|string',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{

            $find = Permission::where('name', 'like',  $request->name)->first();
            if (isset($find)) {
                return 1;
            }else{
                $permiso = new Permission;
                $permiso->name = $request->name;
                $permiso->description = $request->description;
                $save = $permiso->save();
                if (!$save) {
                    return 500;
                }else{
                    return 200;
                }
            }
        }
    }

    public function show(permisos $permisos)
    {
        //
    }

    public function edit(permisos $permisos)
    {
        //
    }

    public function update(Request $request, permisos $permisos)
    {
        //
    }

    public function destroy(permisos $permisos)
    {
        //
    }
}
