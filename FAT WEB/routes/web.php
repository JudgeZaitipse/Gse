<?php

use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

// ruta de verificacion de correo 
Route::get('/email/verify', function () {
    return view('auth.verify');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();
    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');


Route::post('/codigo/{user}', [App\Http\Controllers\Auth\LoginController::class,'verificar'])->name('codigo.Qr');

// sesiones de usuario 
Route::get('/sesiones', function () { return view('usuarios.sesiones'); });
Route::get('/validar/session', [App\Http\Controllers\UserController::class,'validacion']);



// middleware de verificacion 
Route::middleware(['auth','verified','lastLogin'])->group(function () {

    // index 
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');

    // users
    Route::resource('/permisos', App\Http\Controllers\PermisosController::class);
    Route::resource('/roles',    App\Http\Controllers\RolesController::class);
    Route::resource('/usuarios', App\Http\Controllers\UserController::class);

    // clients
    Route::resource('/clientes', App\Http\Controllers\ClienteController::class);
    Route::get('/clientes/list', [App\Http\Controllers\ClienteController::class, 'show']);
    Route::resource('/catalogo', App\Http\Controllers\CatalogoController::class);

    // pedidos
    Route::resource('/pedidos', App\Http\Controllers\PedidosController::class);
    // Route::get('/pedidos', [App\Http\Controllers\PedidosController::class, 'pedidos'])->name('pedidos');

    // productos 
    Route::resource('/productos', App\Http\Controllers\ProductosController::class);
    
});








