<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;
use Auth;

class UserController extends Controller
{

    // funcion session refresh
    public function validacion() {
        $user = User::find(Auth::user()->id);   
        $user->last_login = date('Y-m-d H:i:s');
        $user->save();
        return response()->json(200);
    }

    // informacion de la vistas 
    public function index()
    {
        $role = Role::select('id','name')->get(); 
        $users = User::with(['roles'])->get();
        return view('usuarios.index')->with(['role' => $role , 'users' => $users]);
    }

    // creacion de  usuarios 
    public function store(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'userName'  => 'required|string',
            'password'  => 'required|string',
            'email'     => 'required|email',
            'role'      => 'required',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $find = User::where('name',$request->userName)->first();
            if (isset($find)) {
                return 1;
            }else{
                $user = new User;
                $user->name = $request->userName;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $save = $user->save();
                if($save){
                    $user->assignRole($request->role['name']);
                    return 200;
                }else{
                    return 500;
                }
            }
        }
    }

    // actualizacion de datos 
    public function update(Request $request)
    {
        $v = \Validator::make($request->all(), [
            'userName'  => 'required|string',
            'email'     => 'required|email',
            'role'      => 'required',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $user = User::with(['roles'])->find($request->id);
            if (isset($user)) {
                $user->name = $request->userName;
                $user->email = $request->email;
                if (isset($request->password)) {
                    $user->password = Hash::make($request->password);
                }
                $save = $user->update();
                if($save){
                    foreach ($user->roles as $key => $value) {
                        $user->removeRole($value->name);
                    }
                    foreach ($request->role as $key => $value) {
                        if (!isset($value['name'])) {
                            $user->assignRole($value); 
                        }else{
                            $user->assignRole($value['name']);             
                        }
                    }
                    return 200;
                }else{
                    return 500;
                }
            }else{
                return 1;
            }
        }
    }
    // eliminar registro
    public function destroy($user)
    {
        $user = User::with(['roles'])->find($user);
        if(!empty($user)){
            foreach ($user->roles as $key => $value) {
                $user->removeRole($value->name);
            }
            $user->delete();
            return 200;
        }else{
            return 500;
        }
    }
}
