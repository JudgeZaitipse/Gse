<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Cookie;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function index()
    {  
        // creacion de Cookie  
        $role = Auth::user()->roles()->first()->id; 
        $ip   = \Request::ip();
        if( $role  == 1 && $ip == '127.0.0.1') {
             Cookie::queue('origin_sesion','Nueva Cookie','60');
        } 
        return view('home');
    }

    public function dashboard()
    {
        return view('dashboard.index');
    }
}
