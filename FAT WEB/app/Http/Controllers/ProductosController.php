<?php

namespace App\Http\Controllers;

use App\Models\Productos;
use Illuminate\Http\Request;
use App\Models\Marcas;
use App\Models\Peliculas;
use App\Models\Estados;

class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marcas = Marcas::all('id','nombre');
        $peliculas = Peliculas::all('id','marca','formato','iso');
        $estados = Estados::all('id','nombre');
        $list = Productos::with(['marcas','estados','peliculas'])->get();
        return view('productos.index',compact('marcas', 'list','estados','peliculas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);

        $v = \Validator::make($request->all(), [
            'nombre'      => 'required|string',
            'modelo'      => 'required|string',
            'soporte'     => 'required|string',
            'description' => 'required|string',
            'valor'       => 'required',
            'marca'       => 'required',
            'estado'      => 'required',
            'pelicula'    => 'required',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $find = Productos::where('nombre',$request->nombre)->first();
            if (isset($find)) {
                return 1;
            }else{
                $Productos = new Productos;
                $Productos->nombre = $request->nombre;
                $Productos->modelo = $request->modelo;
                $Productos->soporte = $request->soporte;
                $Productos->description = $request->description;
                $Productos->marca_id = $request->marca['id'];
                $Productos->pelicula_id = $request->pelicula['id'];
                $Productos->estado_id = $request->estado['id'];
                $Productos->valor = $request->valor;
                $save = $Productos->save();
                if($save){
                    return 200;
                }else{
                    return 500;
                }
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit(Productos $productos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Productos $productos)
    {
        $v = \Validator::make($request->all(), [
            'nombre'      => 'required|string',
            'modelo'      => 'required|string',
            'soporte'     => 'required|string',
            'description' => 'required|string',
            'valor'       => 'required',
            'marca'       => 'required',
            'estado'      => 'required',
            'pelicula'    => 'required',
        ]);

        if ($v->fails())
        {
            return response()->json($v->errors()); 
        }else{
            $producto = Productos::find($request->id);
            if (isset($producto)) {
                $producto->nombre = $request->nombre;
                $producto->modelo = $request->modelo;
                $producto->soporte = $request->soporte;
                $producto->description = $request->description;
                $producto->marca_id = $request->marca['id'];
                $producto->pelicula_id = $request->pelicula['id'];
                $producto->estado_id = $request->estado['id'];
                $producto->valor = $request->valor;
                $save = $producto->update();
                if($save){
                    return 200;
                }else{
                    return 500;
                }
            }else{
                return 1;
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy($producto)
    {
        $producto = Productos::find($producto);
        if(!empty($producto)){
            $producto->delete();
            return 200;
        }else{
            return 500;
        }
    }
}
