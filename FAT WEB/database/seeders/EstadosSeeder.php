<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Estados;

class EstadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estados::create(['nombre' => 'Activo','description' => 'Estado activo general']);
        Estados::create(['nombre' => 'Inactivo','description' => 'Estado inactivo general']);
        Estados::create(['nombre' => 'Disponible','description' => 'Estado disponible']);
        Estados::create(['nombre' => 'Alquilado','description' => 'Estado alquilado a un cliente']);
        Estados::create(['nombre' => 'Retraso','description' => 'Estado retrasado en la devolucion']);
        Estados::create(['nombre' => 'Reparacion','description' => 'Estado en reparacion no disponible']);
    }
}